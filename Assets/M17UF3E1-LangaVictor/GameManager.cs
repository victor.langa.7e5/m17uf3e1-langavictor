using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    static GameManager _instance;
    public static GameManager Instance { get => _instance; }

    [SerializeField] int _totalCoins;
    int _coinsCount = 0;

    [SerializeField] Button _button;
    [SerializeField] TextMeshProUGUI _text;
    [SerializeField] TextMeshProUGUI _textScore;


    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(gameObject);
            
        _instance = this;
    }

    private void Start() => _textScore.text = _coinsCount + " / " + _totalCoins;

    private void Update()
    {
        int d = (int)(Time.timeSinceLevelLoad * 100.0f);
        int minutes = d / (60 * 100);
        int seconds = d % (60 * 100) / 100;
        int hundredths = d % 100;
        _text.text = string.Format("{0:00}:{1:00}.{2:00}", minutes, seconds, hundredths);
    }

    public void AddCoin()
    {
        _coinsCount++;
        if (_coinsCount >= _totalCoins)
            EndGame();

        _textScore.text = _coinsCount + " / " + _totalCoins;
    }

    void EndGame()
    {
        _button.gameObject.SetActive(true);

        Time.timeScale = 0;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void LoadScene()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
