using UnityEngine;

public class Coin : MonoBehaviour
{
    private void Update()
    {
        gameObject.transform.localEulerAngles += new Vector3(0, Time.deltaTime * 100, 0);
    }

    private void OnTriggerEnter()
    {
        GameManager.Instance.AddCoin();
        Destroy(gameObject);
    }
}
